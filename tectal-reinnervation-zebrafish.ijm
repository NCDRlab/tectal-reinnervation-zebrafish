//Set working directory & create folder to save ROI
workdir=getDirectory("Choose the image directory") ;
if (File.isDirectory(workdir+"ROI")==0){
File.makeDirectory(workdir+"ROI");
}

if (File.isDirectory(workdir+"output_Area")==0){    
File.makeDirectory(workdir+"output_Area");    
}    
outputdir= workdir+"output_Area"+File.separator;   


run("Set Measurements...", "area mean min perimeter integrated median skewness area_fraction display redirect=None decimal=0");
run("Clear Results");


//Remove elements that are not images    
list = getFileList(workdir);
Images = list;    
    
for (j=0; j<Images.length; j++) {    
    //Compatible image types: .tif, .tiff, .oib, czi    
    if (endsWith(Images[j], ".tif")== false && endsWith(Images[j], ".tiff")== false && endsWith(Images[j], ".oib") == false && endsWith(Images[j], ".czi") == false) {    
        a1 = Array.slice(Images,0, j);    
        a2 = Array.slice(Images, j+1, Images.length);    
        Images = Array.concat(a1, a2);    
        j = j-1;    
    }    
}

print("Number of files to analyse: "+Images.length);

//Select starting image, number of ROIs and re-evaluation
Dialog.create("Image Selection - Detect Area");
Dialog.addMessage("Which image do you want to process?");
Dialog.addChoice("Images", Images);
Dialog.addNumber("Number of ROIs", 1)
Dialog.addCheckbox("Re-analysis (ROI completed)", false); 
Dialog.show();

currentimage = Dialog.getChoice();
nROI = Dialog.getNumber(); 
Rerun = Dialog.getCheckbox(); 


//Let the user name the Regions of interest
Dialog.create("Name the ROIs");    
for (i = 0; i < nROI; i++){
	if (i == 0 ){
		//orignal script purpose: GCL & INL quantification
		Dialog.addString("ROI Name"+i+1, "InnervatedArea");	
	} else if (i== 1){
		//orignal script purpose: GCL & INL quantification
		Dialog.addString("ROI Name"+i+1, "OT-IA");
	} else {
		Dialog.addString("ROI Name"+i+1, "ROI"+i+1);
	}
}
Dialog.show(); 

ROINames = newArray(nROI);
for (i = 0; i < nROI; i++){
	ROINames[i] = Dialog.getString();	
}

for (j=0; j<Images.length; j++) {
//Clear ROI manager
roiManager("reset");

//Let user open image
if (Rerun == true){
	setBatchMode(true);
	currentimage = File.getName(workdir + Images[j]);    
    currentimageAbr=substring(currentimage, 0, indexOf(currentimage, "."));

    open(workdir+File.separator+currentimage);
	
}else{
	open(workdir+currentimage);

	//Get image details
	currentimage = getTitle();
	currentimageAbr=substring(currentimage, 0, indexOf(currentimage, "."));
}


print("Now processing "+currentimageAbr+"...");
run("8-bit");

setForegroundColor(0, 0, 0);
setBackgroundColor(255, 255, 255);

//get image name
currentimage = getTitle();

//Let the user create the outlines of interest
selectWindow(currentimage);
setTool("polygon");
roiManager("Show All with labels");
if (File.exists(workdir+File.separator+"ROI"+File.separator+currentimageAbr+".zip")==1){
open(workdir+File.separator+"ROI"+File.separator+currentimageAbr+".zip");
run("From ROI Manager");
	if (nROI != roiManager("count")){
	Rerun = false;
	print("Too many/few ROIs present. Please adjust.");
	}
} else {
	Rerun = false;
	print("No previously made ROI found.");    
}

if (Rerun == false){
setBatchMode(false);
for (z=0; z < nROI; z++){

waitForUser("Outline Area of "+ROINames[z]+". \nPress ctrl+t to save the ROI");
roiManager("Show All with labels");
roiManager("select", z);
roiManager("Rename", ROINames[z]);
roiManager("Deselect");
run("Select None");
}
}

roiManager("Save", workdir+File.separator+"ROI"+File.separator+currentimageAbr+".zip");


//Process the image: detect area
//Duplicate image to create working copy
selectWindow(currentimage);
run("Duplicate...", "title=[random]");
rename(currentimageAbr + "_Copy");

selectWindow(currentimageAbr + "_Copy");
run("Invert");
run("Invert LUT");


//Do for every outline
for (x = 0; x < nROI; x++){
	
selectWindow(currentimageAbr + "_Copy");
run("Duplicate...", "title=[random]");
rename(currentimageAbr + "_"+ROINames[x]);


selectWindow(currentimageAbr + "_"+ROINames[x]);
run("Duplicate...", "title=[random]");
rename(currentimageAbr + "_Axons"+ROINames[x]);
roiManager("Select", x);
run("Clear Outside");
roiManager("Deselect");
run("Select None");

//Quantify area of the optic tectum (OT)
selectWindow(currentimageAbr + "_"+ROINames[x]);
roiManager("Select", x);
run("Fill");
run("Clear Outside");
roiManager("Deselect");
run("Select None");
run("Analyze Particles...", "size=0-Infinity circularity=0.00-1.00 show=Nothing summarize");

//Quantify area of the axons
selectWindow(currentimageAbr + "_Axons"+ROINames[x]);
setBatchMode("show");
setAutoThreshold("Default");
run("Threshold...");
setThreshold(1, 120);
waitForUser("Set the threshold for the axons, press APPLY, then press OK. \nMake sure that the area you want to include is shown in red");
run("Analyze Particles...", "size=0-Infinity circularity=0.00-1.00 show=Nothing summarize");

close(currentimageAbr + "_"+ROINames[x]);

close(currentimageAbr + "_Copy");
}

run("Images to Stack", "name=Stack title=[] use");
run("Make Composite", "display=Composite");

saveAs("TIFF", outputdir+currentimageAbr+"_Final");

while (nImages!= 0){   
   		 close();  
   	}   

if (Rerun == false){
	if (j < Images.length-1){
	Dialog.create("Image Selection - Detect Mean Grey value");
	Dialog.addMessage("Which image do you want to process?");
	Dialog.addChoice("Images", list);
	Dialog.addCheckbox("Re-analysis (ROI completed)", false); 
	Dialog.show();
	currentimage = Dialog.getChoice();
	Rerun = Dialog.getCheckbox(); 
	}
}


}

//Signify end of macro
print("Job's done!")
;


